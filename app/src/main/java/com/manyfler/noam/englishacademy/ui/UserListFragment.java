package com.manyfler.noam.englishacademy.ui;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.manyfler.noam.englishacademy.R;
import com.manyfler.noam.englishacademy.databinding.ListFragmentBinding;
import com.manyfler.noam.englishacademy.viewmodel.UserListViewModel;

/**
 * The user list fragment. holds user adapter
 */
public class UserListFragment extends android.support.v4.app.Fragment {
    public  static final String TAG = "UserListFragment";

    private UserAdapter mUserAdapter;

    private ListFragmentBinding mBinding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.list_fragment, container, false);
        mUserAdapter = new UserAdapter(mUserClickCallback);
        mBinding.usersList.setAdapter(mUserAdapter);

        return mBinding.getRoot();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final UserListViewModel viewModel =
                ViewModelProviders.of(this).get(UserListViewModel.class);

        subscribeUi(viewModel);
    }

    private void subscribeUi(UserListViewModel viewModel) {
        // Update the list when the data changes
        viewModel.getUsers().observe(this, myUsers -> {
            if (myUsers != null) {
                mBinding.setIsLoading(false);
                mUserAdapter.setUserList(myUsers);
            } else {
                mBinding.setIsLoading(true);
            }
            mBinding.executePendingBindings();
        });
    }

    private final UserClickCallback mUserClickCallback = user -> {
        if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
            if (getActivity() != null) {
                ((UsersActivity) getActivity()).show(user);
            }
        }
    };
}
