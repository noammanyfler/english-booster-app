package com.manyfler.noam.englishacademy.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.manyfler.noam.englishacademy.R;
import com.manyfler.noam.englishacademy.model.User;

/**
 * Created by noamm on 19-02-18.
 */

public class UsersActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);

        // Add product list fragment if this is first creation
        if (savedInstanceState == null) {
            UserListFragment fragment = new UserListFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.users_fragment_container, fragment, UserListFragment.TAG).commit();
        }
    }

    public void show(User user) {
        UserFragment userFragment = UserFragment.forUser(user.getUid());
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("users")
                .replace(R.id.users_fragment_container,
                        userFragment, null).commit();
    }
}
