package com.manyfler.noam.englishacademy.db.dao;

/**
 * Created by noamm on 25-02-18.
 */

public interface Callback<T> {
    void onComplete(T data);
}
