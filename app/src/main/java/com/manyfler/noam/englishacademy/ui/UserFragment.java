package com.manyfler.noam.englishacademy.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.manyfler.noam.englishacademy.R;
import com.manyfler.noam.englishacademy.databinding.UserFragmentBinding;
import com.manyfler.noam.englishacademy.viewmodel.UserViewModel;

/**
 * Created by noamm on 19-02-18.
 */

public class UserFragment extends Fragment {
    private static final String KEY_USER_ID = "user_id";
    private UserFragmentBinding mBinding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate this data binding layout
        mBinding = DataBindingUtil.inflate(inflater, R.layout.user_fragment, container, false);

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        UserViewModel.Factory factory = new UserViewModel.Factory(
                getActivity().getApplication(), getArguments().getString(KEY_USER_ID));

        final UserViewModel model = ViewModelProviders.of(this, factory)
                .get(UserViewModel.class);

        mBinding.setUserViewModel(model);

        subscribeToModel(model);
    }

    private void subscribeToModel(final UserViewModel model) {
        // Observe product data
        model.getObservableUser().observe(this, model::setUser);
    }

    /** Creates user fragment for specific user ID */
    public static UserFragment forUser(String userUid) {
        UserFragment fragment = new UserFragment();
        Bundle args = new Bundle();
        args.putString(KEY_USER_ID, userUid);
        fragment.setArguments(args);
        return fragment;
    }
}
