package com.manyfler.noam.englishacademy.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;

import com.manyfler.noam.englishacademy.BasicApp;
import com.manyfler.noam.englishacademy.db.entity.PicEntity;

import java.util.List;

/**
 * Created by noamm on 28-02-18.
 */

public class PicGameViewModel extends AndroidViewModel {
    private final MediatorLiveData<List<PicEntity>> mObservablePics;
    private Integer index;
    public Boolean isCreatedPicFragment = false;

    public PicGameViewModel(Application application) {
        super(application);

        mObservablePics = new MediatorLiveData<>();
        mObservablePics.setValue(null);
        LiveData<List<PicEntity>> allPics = ((BasicApp) application).getRepository().getAllPics();
        mObservablePics.addSource(allPics, mObservablePics::setValue);

        index = 0;
    }

    public LiveData<List<PicEntity>> getPics() {
        return mObservablePics;
    }

    public Integer getIndex() {
        return this.index;
    }

    public PicEntity nextPicture() {
        if (this.index < this.getPics().getValue().size() - 1) {
            this.index++;
            return this.getPics().getValue().get(this.index);
        }else {
            return null;
        }
    }
}
