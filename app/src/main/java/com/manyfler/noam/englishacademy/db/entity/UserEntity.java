package com.manyfler.noam.englishacademy.db.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.manyfler.noam.englishacademy.model.User;

import java.util.HashMap;

/**
 * Created by noamm on 16-02-18.
 */
@Entity(tableName = "users")
public class UserEntity implements User{
    @PrimaryKey
    @NonNull
    private String uid;
    private String email;
    private int score;
    private float lastUpdated;
    private int isAdmin;


    public UserEntity() {
        this.setUid("");
        this.setEmail("");
        this.setScore(0);
        this.setLastUpdated(System.currentTimeMillis());
        this.isAdmin = 0;
    }
    @Ignore
    public UserEntity(String uid, String email, int score, float lastUpdated, Integer isAdmin) {
        this.uid = uid;
        this.email = email;
        this.score = score;
        this.lastUpdated = lastUpdated;
        this.isAdmin = isAdmin;
    }

    @Ignore
    public UserEntity(User user) {
        this.uid = user.getUid();
        this.email = user.getEmail();
        this.score = user.getScore();
        this.lastUpdated = user.getLastUpdated();
        this.isAdmin = user.getIsAdmin();
    }

    @Override
    public String getUid() {
        return this.uid;
    }

    public void setUid(@NonNull String uid) {
        this.uid = uid;
    }

    @Override
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int getScore() {
        return this.score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public float getLastUpdated() {
        return this.lastUpdated;
    }

    public void setLastUpdated(float lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public Integer getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(int isAdmin) {
        this.isAdmin = isAdmin;
    }

    public HashMap<String,Object> toJson() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("uid", getUid());
        result.put("email", getEmail());
        result.put("score", getScore());
        result.put("lastUpdated", getLastUpdated());
        result.put("isAdmin", getIsAdmin());
        return result;
    }
}
