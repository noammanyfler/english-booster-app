package com.manyfler.noam.englishacademy.model;

/**
 * Created by noamm on 16-02-18.
 */

public interface Pic {
    String getUid();
    String getPictureUrl();
    String getHebrew();
    String getEnglish();
    float getLastUpdated();
}
