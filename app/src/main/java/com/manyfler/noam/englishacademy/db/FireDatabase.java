package com.manyfler.noam.englishacademy.db;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.manyfler.noam.englishacademy.db.dao.firebaseDao.PicDaoFirebase;
import com.manyfler.noam.englishacademy.db.dao.firebaseDao.UserDaoFirebase;

/**
 * Created by noamm on 16-02-18.
 */

public class FireDatabase {
    private static FireDatabase sInstance;
    private FirebaseDatabase firebaseDatabase;
    private FirebaseStorage firebaseStorage;
    private final UserDaoFirebase mUserDaoFirebase;
    private final PicDaoFirebase mPicDaoFirebase;

    private FireDatabase() {
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseStorage = FirebaseStorage.getInstance();
        mUserDaoFirebase = UserDaoFirebase.getInstance(firebaseDatabase);
        mPicDaoFirebase = PicDaoFirebase.getInstance(firebaseDatabase, firebaseStorage);
    }

    public static FireDatabase getInstance() {
        if (sInstance == null) {
            synchronized (FirebaseDatabase.class) {
                if (sInstance == null) {
                    sInstance = new FireDatabase();
                }
            }
        }
        return sInstance;
    }

    public UserDaoFirebase getUserDaoFirebase() {
        return mUserDaoFirebase;
    }

    public PicDaoFirebase getPicDaoFirebase() {
        return mPicDaoFirebase;
    }
}
