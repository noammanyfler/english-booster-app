package com.manyfler.noam.englishacademy.db.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.manyfler.noam.englishacademy.model.Pic;

/**
 * Created by noamm on 24-02-18.
 */

@Entity(tableName = "pics")
public class PicEntity implements Pic{
    @PrimaryKey
    @NonNull
    private String uid;
    private String pictureUrl;
    private String hebrew;
    private String english;
    private float lastUpdated;

    @Ignore
    private Bitmap bitmap;

    public PicEntity() {
        this.setUid("");
        this.setEnglish("");
        this.setHebrew("");
        this.setLastUpdated(System.currentTimeMillis());
        this.setPictureUrl("");
        this.setBitmap(null);
    }

    @Ignore
    public PicEntity(Pic pic) {
        this.uid = pic.getUid();
        this.english = pic.getEnglish();
        this.lastUpdated = pic.getLastUpdated();
        this.hebrew = pic.getHebrew();
        this.pictureUrl = pic.getPictureUrl();
        this.bitmap = null;
    }

    @Ignore
    public PicEntity(@NonNull String uid, String pictureUrl, String hebrew, String english, float lastUpdated) {
        this.uid = uid;
        this.pictureUrl = pictureUrl;
        this.hebrew = hebrew;
        this.english = english;
        this.lastUpdated = lastUpdated;
        this.bitmap = null;
    }

    @NonNull
    @Override
    public String getUid() {
        return this.uid;
    }

    public void setUid(@NonNull String uid) {
        this.uid = uid;
    }

    @Override
    public String getPictureUrl() {
        return this.pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    @Override
    public String getHebrew() {
        return this.hebrew;
    }

    public void setHebrew(String hebrew) {
        this.hebrew = hebrew;
    }

    @Override
    public String getEnglish() {
        return this.english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    @Override
    public float getLastUpdated() {
        return this.lastUpdated;
    }

    public void setLastUpdated(float lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Ignore
    public Bitmap getBitmap() {
        return this.bitmap;
    }

    @Ignore
    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
