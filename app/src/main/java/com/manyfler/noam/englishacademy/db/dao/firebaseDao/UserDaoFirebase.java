package com.manyfler.noam.englishacademy.db.dao.firebaseDao;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.manyfler.noam.englishacademy.BasicApp;
import com.manyfler.noam.englishacademy.db.dao.Callback;
import com.manyfler.noam.englishacademy.db.entity.UserEntity;
import com.manyfler.noam.englishacademy.model.User;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by noamm on 19-02-18.
 */

public class UserDaoFirebase {

    private final FirebaseDatabase mFirebaseDatabase;
    private static UserDaoFirebase sInstance;

    private UserDaoFirebase(FirebaseDatabase mFirebaseDatabase) {
        this.mFirebaseDatabase = mFirebaseDatabase;
    }

    public static UserDaoFirebase getInstance(FirebaseDatabase firebaseDatabase) {
        if (sInstance == null) {
            synchronized (UserDaoFirebase.class) {
                if (sInstance == null) {
                    sInstance = new UserDaoFirebase(firebaseDatabase);
                }
            }
        }
        return sInstance;
    }

    public void getAllUsersAndObserve(final Callback<List<User>> callback) {
        SharedPreferences sharedPreferences = BasicApp.getContext().getSharedPreferences("english_academy", Context.MODE_PRIVATE);
        float usersLastUpdated = sharedPreferences.getFloat("usersLastUpdated", 0);
        DatabaseReference myRef = this.mFirebaseDatabase.getReference("users");
        Query query = myRef.orderByChild("lastUpdated").startAt(usersLastUpdated);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<User> list = new LinkedList<>();
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    UserEntity user = snap.getValue(UserEntity.class);
                    list.add(user);
                }
                callback.onComplete(list);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onComplete(null);
            }
        });
    }

    public void insertUser(final User user, DatabaseReference.CompletionListener completionListener) {
        DatabaseReference myRef = this.mFirebaseDatabase.getReference("users");
        myRef.child(user.getUid()).setValue(user, completionListener);
    }

    public void deleteUser(final String uid, DatabaseReference.CompletionListener completionListener) {
        DatabaseReference myRef = this.mFirebaseDatabase.getReference("users").child(uid);
        myRef.removeValue();
    }

    public void addScore(UserEntity userEntity) {
        DatabaseReference myRef = this.mFirebaseDatabase.getReference("users").child(userEntity.getUid());
        Map<String, Object> userUpdates = new HashMap<>();
        userUpdates.put("lastUpdated", System.currentTimeMillis());
        userUpdates.put("score", userEntity.getScore() + 1);
        myRef.updateChildren(userUpdates);
    }
}
