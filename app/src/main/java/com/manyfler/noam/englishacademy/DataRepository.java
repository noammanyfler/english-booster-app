package com.manyfler.noam.englishacademy;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.manyfler.noam.englishacademy.db.AppDatabase;
import com.manyfler.noam.englishacademy.db.FireDatabase;
import com.manyfler.noam.englishacademy.db.PicDataStorage;
import com.manyfler.noam.englishacademy.db.entity.PicEntity;
import com.manyfler.noam.englishacademy.db.entity.UserEntity;
import com.manyfler.noam.englishacademy.model.Pic;
import com.manyfler.noam.englishacademy.model.User;

import java.util.List;

/**
 * Created by noamm on 16-02-18.
 */

public class DataRepository {
    private final static String TAG = "DataRepository";
    private static DataRepository sInstance;

    private final AppExecutors mAppExecutors;
    private final AppDatabase mDatabase;
    private final FireDatabase mFireDatabase;
    private final PicDataStorage mPicDataStorage;

    private MediatorLiveData<List<UserEntity>> mObservableUsers;
    private MediatorLiveData<List<PicEntity>> mObservablePics;

    private DataRepository(final AppDatabase database, FireDatabase fireDatabase, PicDataStorage picDataStorage, AppExecutors appExecutors) {
        mDatabase = database;
        mFireDatabase = fireDatabase;
        mAppExecutors = appExecutors;
        mPicDataStorage = picDataStorage;

        mObservableUsers = new MediatorLiveData<>();
        mObservableUsers.addSource(mDatabase.userDao().loadAllUsers(),
                userEntities -> {
                    if (mDatabase.getDatabaseCreated().getValue() != null) {
                        mObservableUsers.postValue(userEntities);
                    }
                });

        mObservablePics = new MediatorLiveData<>();
        mObservablePics.addSource(mDatabase.picDao().loadAllPictures(), picEntities -> {
            if (mDatabase.getDatabaseCreated().getValue() != null) {
                mObservablePics.postValue(picEntities);
            }
        });
    }

    public static DataRepository getInstance(final AppDatabase database, FireDatabase fireDatabase, PicDataStorage picDataStorage, AppExecutors mAppExecutors) {
        if (sInstance == null) {
            synchronized (DataRepository.class) {
                if (sInstance == null) {
                    sInstance = new DataRepository(database, fireDatabase, picDataStorage, mAppExecutors);
                }
            }
        }
        return sInstance;
    }

    /**
     * Get the list of products from the database and get notified when the data changes
     */
    public LiveData<List<UserEntity>> getAllUsers() {
        mAppExecutors.networkIO().execute(() -> mFireDatabase.getUserDaoFirebase().getAllUsersAndObserve(data -> {
            mAppExecutors.diskIO().execute(() -> {
                SharedPreferences sharedPreferences = BasicApp.getContext().getSharedPreferences("english_academy", Context.MODE_PRIVATE);
                float usersLastUpdated = sharedPreferences.getFloat("usersLastUpdated", 0);

                if (data != null && data.size() > 0) {
                    float recentUpdate = usersLastUpdated;

                    for (User user : data) {
                        mDatabase.userDao().insertAll(new UserEntity(user));
                        if (user.getLastUpdated() > recentUpdate) {
                            recentUpdate = user.getLastUpdated();
                        }
                    }

                    SharedPreferences.Editor editor = BasicApp.getContext().getSharedPreferences("english_academy", Context.MODE_PRIVATE).edit();
                    editor.putFloat("usersLastUpdated", recentUpdate);
                    editor.apply();
                }
            });
        }));

        return mObservableUsers;
    }

    public LiveData<UserEntity> getUser(final String uid) {
        return mDatabase.userDao().loadUser(uid);
    }

    public void insertUser(User user) {
        mAppExecutors.networkIO().execute(() -> mFireDatabase.getUserDaoFirebase().insertUser(user, (databaseError, databaseReference) -> {
        }));
    }

    public void deleteUser(String uid) {
        mAppExecutors.networkIO().execute(() -> mFireDatabase.getUserDaoFirebase().deleteUser(uid,
                (databaseError, databaseReference) -> {
//            mAppExecutors.diskIO().execute(() -> mDatabase.userDao().deleteUsers(mDatabase.userDao().loadUser(uid).getValue()))
                }));
    }

    public void addScore(FirebaseUser user) {
        mAppExecutors.diskIO().execute(() -> {
            UserEntity userEntity = this.mDatabase.userDao().getUser(user.getUid());
            mAppExecutors.networkIO().execute(() -> {
                this.mFireDatabase.getUserDaoFirebase().addScore(userEntity);
            });
        });
    }

    public LiveData<List<PicEntity>> getAllPics() {
        mAppExecutors.networkIO().execute(() -> mFireDatabase.getPicDaoFirebase().getAllPicturesAndObserve(data -> {
            mAppExecutors.diskIO().execute(() -> {
                SharedPreferences sharedPreferences = BasicApp.getContext().getSharedPreferences("english_academy", Context.MODE_PRIVATE);
                float picsLastUpdated = sharedPreferences.getFloat("picsLastUpdated", 0);

                if (data != null && data.size() > 0) {
                    float recentUpdate = picsLastUpdated;

                    for (Pic pic: data) {
                        PicEntity picEntity = new PicEntity(pic);
                        mDatabase.picDao().insertAll(picEntity);

                        if (pic.getLastUpdated() > recentUpdate) {
                            recentUpdate = pic.getLastUpdated();
                        }
                    }

                    SharedPreferences.Editor editor = BasicApp.getContext().getSharedPreferences("english_academy", Context.MODE_PRIVATE).edit();
                    editor.putFloat("picsLastUpdated", recentUpdate);
                    editor.apply();
                }
            });
        }));

        return mObservablePics;
    }

    public LiveData<PicEntity> getPic(final String uid) {
        LiveData<PicEntity> pic = mDatabase.picDao().loadPic(uid);
        MediatorLiveData<PicEntity> picEntityMediatorLiveData = new MediatorLiveData<>();
        picEntityMediatorLiveData.addSource(pic, picEntity -> {
            getImage(picEntity.getPictureUrl(), new GetImageListener() {
                @Override
                public void onSuccess(Bitmap image) {
                    picEntity.setBitmap(image);
                    picEntityMediatorLiveData.postValue(picEntity);
                }

                @Override
                public void onFail() {
                    Log.d(TAG, "onFail: Failed...");
                }
            });
        });

        return picEntityMediatorLiveData;
    }

    public void addPic(PicEntity picEntity, DatabaseReference.CompletionListener listener) {
        mAppExecutors.networkIO().execute(() -> {
            this.mFireDatabase.getPicDaoFirebase().insertPic(picEntity, (databaseError, databaseReference) -> {
                Log.d(TAG, "onComplete: Saved pic to firebase storage");
                listener.onComplete(databaseError, databaseReference);
            });
        });
    }

    public interface GetImageListener{
        void onSuccess(Bitmap image);
        void onFail();
    }

    public void getImage(final String url, final GetImageListener listener) {
        mAppExecutors.diskIO().execute(() -> {
            //check if image exist locally
            String fileName = url;
            Bitmap image = mPicDataStorage.loadImageFromFile(fileName);

            if (image != null){
                Log.d(TAG,"getImage from local success " + fileName);
                listener.onSuccess(image);
            }else {
                mAppExecutors.networkIO().execute(() -> this.mFireDatabase.getPicDaoFirebase().getImage(url, new GetImageListener() {
                    @Override
                    public void onSuccess(Bitmap image) {
                        String fileName = url;
                        Log.d(TAG,"getImage from FB success " + fileName);
                        mPicDataStorage.saveImageToFile(image, fileName);
                        listener.onSuccess(image);
                    }

                    @Override
                    public void onFail() {
                        Log.d(TAG,"getImage from FB fail ");
                        listener.onFail();
                    }
                }));
            }
        });
    }

}
