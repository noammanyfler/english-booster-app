package com.manyfler.noam.englishacademy.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import com.manyfler.noam.englishacademy.DataRepository;
import com.manyfler.noam.englishacademy.db.entity.PicEntity;
import com.manyfler.noam.englishacademy.model.Pic;

/**
 * Created by noamm on 05-03-18.
 */

public class UploadPicViewModel extends AndroidViewModel {
    private PicEntity mPic;
    private DataRepository mRepository;

    public UploadPicViewModel(@NonNull Application application, DataRepository repository) {
        super(application);
        mRepository = repository;
        this.mPic = new PicEntity();
    }

    public void setPic(PicEntity pic) {
        this.mPic = pic;
    }
    public Pic getPic() {
        return this.mPic;
    }
}
