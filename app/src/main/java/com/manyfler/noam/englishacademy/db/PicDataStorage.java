package com.manyfler.noam.englishacademy.db;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.manyfler.noam.englishacademy.BasicApp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by noamm on 25-02-18.
 */

public class PicDataStorage {
    private final static String TAG = "PicDataStorage";

    private static PicDataStorage sInstance;

    private PicDataStorage() {}

    public static PicDataStorage getInstance() {
        if (sInstance == null) {
            synchronized (PicDataStorage.class) {
                if (sInstance == null) {
                    sInstance = new PicDataStorage();
                }
            }
        }
        return sInstance;
    }

    public Bitmap loadImageFromFile(String imageFileName){
        Bitmap bitmap = null;
        try {
            File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/english-academy-photos");
            File imageFile = new File(dir,imageFileName);
            InputStream inputStream = new FileInputStream(imageFile);
            bitmap = BitmapFactory.decodeStream(inputStream);
            Log.d(TAG,"got image from cache: " + imageFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public void saveImageToFile(Bitmap imageBitmap, String imageFileName){
        String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File myDir = new File(root + "/english-academy-photos");
        myDir.mkdirs();
        File file = new File(myDir, imageFileName);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        // Tell the media scanner about the new file so that it is
        // immediately available to the user.
        MediaScannerConnection.scanFile(BasicApp.getContext(), new String[] { file.toString() }, null,
                (path, uri) -> {
                    Log.i("ExternalStorage", "Scanned " + path + ":");
                    Log.i("ExternalStorage", "-> uri=" + uri);
                });


//            File dir = Environment.getExternalStoragePublicDirectory(
//                    Environment.DIRECTORY_PICTURES);
//            if (!dir.exists()) {
//                dir.mkdir();
//            }
//            File imageFile = new File(dir,imageFileName);
//            imageFile.createNewFile();
//
//            OutputStream out = new FileOutputStream(imageFile);
//            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
//            out.close();
//
//            addPictureToGallery(imageFile);
    }

    private void addPictureToGallery(File imageFile){
        //add the picture to the gallery so we dont need to manage the cache size
        Intent mediaScanIntent = new
                Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(imageFile);
        mediaScanIntent.setData(contentUri);
        BasicApp.getContext().sendBroadcast(mediaScanIntent);
    }
}
