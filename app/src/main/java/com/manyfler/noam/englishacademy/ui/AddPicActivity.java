package com.manyfler.noam.englishacademy.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresPermission;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.manyfler.noam.englishacademy.BasicApp;
import com.manyfler.noam.englishacademy.R;
import com.manyfler.noam.englishacademy.db.entity.PicEntity;
import com.manyfler.noam.englishacademy.viewmodel.UploadPicViewModel;

import java.io.IOException;

/**
 * Created by noamm on 04-03-18.
 */

public class AddPicActivity extends AppCompatActivity {
    private UploadPicViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_pic_activity);


        Button uploadButton = (Button) findViewById(R.id.uploadButton);
        uploadButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(v.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                getFileFromGallery();
            }
        });
        Button submitButton = (Button) findViewById(R.id.submitPicture);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitPicture();
            }
        });

        viewModel = new UploadPicViewModel(this.getApplication(), ((BasicApp)getApplication()).getRepository());
    }

    private void submitPicture() {
        PicEntity picEntity = (PicEntity) this.viewModel.getPic();
        picEntity.setHebrew(((EditText)findViewById(R.id.hebrewInput)).getText().toString());
        picEntity.setEnglish(((EditText)findViewById(R.id.englishInput)).getText().toString());
        picEntity.setUid(((EditText)findViewById(R.id.englishInput)).getText().toString());
        picEntity.setPictureUrl(((EditText)findViewById(R.id.englishInput)).getText().toString()+ ".jpg") ;
        findViewById(R.id.loading_pic_cloud).setVisibility(View.VISIBLE);
        ((BasicApp)getApplication()).getRepository().addPic(picEntity, (databaseError, databaseReference) -> {
            findViewById(R.id.loading_pic_cloud).setVisibility(View.INVISIBLE);
            Snackbar.make(findViewById(R.id.add_pic_activity), "תמונה התווספה בהצלחה", Snackbar.LENGTH_LONG)
                    .setAction("סגור", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    })
                    .addCallback(new Snackbar.Callback() {
                        @Override
                        public void onShown(Snackbar sb) {
                            super.onShown(sb);
                        }

                        @Override
                        public void onDismissed(Snackbar transientBottomBar, int event) {
                            finish();
                        }
                    })
                    .setActionTextColor(getResources().getColor(R.color.colorPrimaryDark ))
                    .show();
        });
    }

    @Override
    @RequiresPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Here we need to check if the activity that was triggers was the Image Gallery.
        // If it is the requestCode will match the LOAD_IMAGE_RESULTS value.
        // If the resultCode is RESULT_OK and there is some data we know that an image was picked.
        if (requestCode == 667 && resultCode == RESULT_OK && data != null) {
            if (data != null) {
                Uri photoUri = data.getData();
                // Do something with the photo based on Uri
                try {
                    Bitmap selectedImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), photoUri);
                    //then create a copy of bitmap bmp1 into bmp2
                    Bitmap bmpForResize = selectedImage.copy(selectedImage.getConfig(), true);

//                    Bitmap resizedBitmap = getResizedBitmap(bmpForResize, 200,200);

                    PicEntity picEntity = new PicEntity(this.viewModel.getPic());
                    picEntity.setBitmap(bmpForResize);
                    this.viewModel.setPic(picEntity);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    @RequiresPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
    void getFileFromGallery() {
        // Create intent for picking a photo from the gallery
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        // If you call startActivityForResult() using an intent that no app can handle, your app will crash.
        // So as long as the result is not null, it's safe to use the intent.
        if (intent.resolveActivity(this.getPackageManager()) != null) {
            // Bring up gallery to select a photo
            startActivityForResult(intent, 667);
        }
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        /*bm.recycle();*/
        return resizedBitmap;
    }
}
