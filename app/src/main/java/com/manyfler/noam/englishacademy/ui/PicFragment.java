package com.manyfler.noam.englishacademy.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.manyfler.noam.englishacademy.R;
import com.manyfler.noam.englishacademy.databinding.PicFragmentBinding;
import com.manyfler.noam.englishacademy.viewmodel.PicViewModel;

/**
 * Created by noamm on 28-02-18.
 */

public class PicFragment extends Fragment {
    private static final String KEY_PIC_ID = "pic_id";
    public static final String TAG = "PicFragment";
    private PicFragmentBinding mBinding;

    public PicViewModel model;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate this data binding layout
        mBinding = DataBindingUtil.inflate(inflater, R.layout.pic_fragment, container, false);
        return mBinding.getRoot();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        PicViewModel.Factory factory = new PicViewModel.Factory(
                getActivity().getApplication(), getArguments().getString(KEY_PIC_ID));

        this.model = ViewModelProviders.of(this, factory)
                .get(PicViewModel.class);
        mBinding.setPicViewModel(model);

        subscribeToModel(model);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button clickButton = view.findViewById(R.id.checkTranslationButton);
        clickButton.setOnClickListener(v -> {
            checkTranslation(view);
            Log.d(TAG, "onViewCreated: Cool stuff");
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void subscribeToModel(final PicViewModel model) {
        // Observe product data
        model.getObservablePic().observe(this, picEntity -> {
            if (picEntity.getBitmap() == null) {
                mBinding.setIsLoading(true);
            } else {
                this.model.setPic(picEntity);
                mBinding.setIsLoading(false);
            }

            mBinding.executePendingBindings();
        });
    }

    /** Creates user fragment for specific user ID */
    public static PicFragment forPic(String picUid) {
        PicFragment fragment = new PicFragment();
        Bundle args = new Bundle();
        args.putString(KEY_PIC_ID, picUid);
        fragment.setArguments(args);
        return fragment;
    }

    public void checkTranslation(View v) {
        EditText editText = v.findViewById(R.id.translation);
        if (!model.getObservablePic().getValue().getEnglish().toLowerCase().equals(
                editText.getText().toString().toLowerCase())) {
            editText.setError("טעות.. נסה שוב");
        } else {
            model.addScoreToUser();
            editText.setTextColor(Color.GREEN);
            FragmentManager ft = getFragmentManager();
            AddScoreFragment addScoreFragment = new AddScoreFragment();
            addScoreFragment.show(ft, AddScoreFragment.TAG);
        }

    }

}
