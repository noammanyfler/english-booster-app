package com.manyfler.noam.englishacademy.db.dao.firebaseDao;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.manyfler.noam.englishacademy.BasicApp;
import com.manyfler.noam.englishacademy.DataRepository;
import com.manyfler.noam.englishacademy.db.dao.Callback;
import com.manyfler.noam.englishacademy.db.entity.PicEntity;
import com.manyfler.noam.englishacademy.model.Pic;

import java.io.ByteArrayOutputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by noamm on 25-02-18.
 */

public class PicDaoFirebase {
    private final FirebaseDatabase mFirebaseDatabase;
    private final FirebaseStorage mFirebaseStorage;
    private static PicDaoFirebase sInstance;

    private PicDaoFirebase(FirebaseDatabase firebaseDatabase, FirebaseStorage firebaseStorage) {
        this.mFirebaseDatabase = firebaseDatabase;
        this.mFirebaseStorage = firebaseStorage;
    }

    public static PicDaoFirebase getInstance(FirebaseDatabase firebaseDatabase, FirebaseStorage firebaseStorage) {
        if (sInstance == null) {
            synchronized (PicDaoFirebase.class) {
                if (sInstance == null) {
                    sInstance = new PicDaoFirebase(firebaseDatabase, firebaseStorage);
                }
            }
        }

        return sInstance;
    }

    public void getAllPicturesAndObserve(final Callback<List<Pic>> callback) {
        SharedPreferences sharedPreferences = BasicApp.getContext().getSharedPreferences("english_academy", Context.MODE_PRIVATE);
        float usersLastUpdated = sharedPreferences.getFloat("picsLastUpdated", 0);
        DatabaseReference myRef = this.mFirebaseDatabase.getReference("pics");
        Query query = myRef.orderByChild("lastUpdated").startAt(usersLastUpdated);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Pic> list = new LinkedList<>();
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    PicEntity pic = snap.getValue(PicEntity.class);
                    list.add(pic);
                }
                callback.onComplete(list);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onComplete(null);
            }
        });
    }

    public void insertPic(final PicEntity pic, DatabaseReference.CompletionListener completionListener) {
        StorageReference httpsReference = this.mFirebaseStorage.getReference(pic.getUid());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        pic.getBitmap().compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = httpsReference.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                Log.d("inserPic", "onFailure: failed...");
            }
        }).addOnSuccessListener(taskSnapshot -> {
            // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
            Uri downloadUrl = taskSnapshot.getDownloadUrl();
        });

        PicEntity picEntity = new PicEntity(pic);
        pic.setBitmap(null);
        DatabaseReference myRef = this.mFirebaseDatabase.getReference("pics");
        myRef.child(pic.getUid()).setValue(pic, completionListener);
    }

    public void getImage(String url, DataRepository.GetImageListener getImageListener) {
        StorageReference httpsReference = this.mFirebaseStorage.getReference(url.substring(0, url.lastIndexOf(".")));
        final long ONE_MEGABYTE = 1024 * 1024;
        httpsReference.getBytes(3* ONE_MEGABYTE).addOnSuccessListener(bytes -> {
            Bitmap image = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
            getImageListener.onSuccess(image);
        }).addOnFailureListener(exception -> {
            Log.d("TAG",exception.getMessage());
            getImageListener.onFail();
        });
    }
}
