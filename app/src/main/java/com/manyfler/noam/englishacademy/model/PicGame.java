package com.manyfler.noam.englishacademy.model;

import android.graphics.Bitmap;

import java.util.List;

/**
 * Created by noamm on 16-02-18.
 */

public interface PicGame {
    String getUid();
    String getHebTitle();
    List<String> getPicUid();
}
