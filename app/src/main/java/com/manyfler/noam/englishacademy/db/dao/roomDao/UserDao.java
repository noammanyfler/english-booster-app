package com.manyfler.noam.englishacademy.db.dao.roomDao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.manyfler.noam.englishacademy.db.entity.UserEntity;

import java.util.List;

/**
 * Created by noamm on 16-02-18.
 */
@Dao
public interface UserDao {
    @Query("SELECT * FROM users where uid =  :uid")
    LiveData<UserEntity> loadUser(String uid);

    @Query("SELECT * FROM users")
    LiveData<List<UserEntity>> loadAllUsers();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(UserEntity... users);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<UserEntity> users);

    @Delete
    void deleteUsers(UserEntity... users);

    @Query("SELECT * FROM users where uid =  :uid")
    UserEntity getUser(String uid);
}
