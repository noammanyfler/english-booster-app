package com.manyfler.noam.englishacademy.model;

/**
 * Created by noamm on 16-02-18.
 */

public interface User {
    String getUid();
    String getEmail();
    int getScore();
    float getLastUpdated();
    Integer getIsAdmin();
}
