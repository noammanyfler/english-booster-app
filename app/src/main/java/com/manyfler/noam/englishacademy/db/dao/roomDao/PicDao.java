package com.manyfler.noam.englishacademy.db.dao.roomDao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.manyfler.noam.englishacademy.db.entity.PicEntity;

import java.util.List;

/**
 * Created by noamm on 24-02-18.
 */
@Dao
public interface PicDao {
    @Query("SELECT * FROM pics where uid =  :uid")
    LiveData<PicEntity> loadPic(String uid);

    @Query("SELECT * FROM pics")
    LiveData<List<PicEntity>> loadAllPictures();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(PicEntity... picEntities);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<PicEntity> pics);
}
