package com.manyfler.noam.englishacademy.ui;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.manyfler.noam.englishacademy.R;
import com.manyfler.noam.englishacademy.databinding.PicGameFragmentBinding;
import com.manyfler.noam.englishacademy.db.entity.PicEntity;
import com.manyfler.noam.englishacademy.model.Pic;
import com.manyfler.noam.englishacademy.viewmodel.PicGameViewModel;

import java.util.List;

/**
 * Created by noamm on 28-02-18.
 */

public class PicGameFragment extends Fragment {
    public static final String TAG = "PicGameFragment";

    List<? extends Pic> mPicList;
    PicGameViewModel viewModel;

    private PicGameFragmentBinding mBinding;

    public final PicSubmitCallback mPicSubmitCallback = pic -> {
        if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
            if (getActivity() != null) {
                ((PicGameActivity) getActivity()).show(pic);
            }
        }
    };

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        ((Button)view.findViewById(R.id.nextPictureButton)).setOnClickListener(v -> this.nextPic());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.pic_game_fragment, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.viewModel =
                ViewModelProviders.of(this).get(PicGameViewModel.class);
        subscribeUi(viewModel);
    }



    private void subscribeUi(PicGameViewModel viewModel) {
        // Update the list when the data changes
        viewModel.getPics().observe(this, myPics -> {
            if (myPics != null) {
                mBinding.setIsLoading(false);
                this.mPicList = myPics;
                if (myPics.size() > 0 && !viewModel.isCreatedPicFragment) {
                    ((PicGameActivity) getActivity()).show(this.viewModel.getPics().getValue().get(this.viewModel.getIndex()));
                    viewModel.isCreatedPicFragment = true;
                }
            } else {
                mBinding.setIsLoading(true);
            }
            mBinding.executePendingBindings();
        });
    }

    public void nextPic() {
        PicEntity pic = this.viewModel.nextPicture();
        if (pic != null) {
            ((PicGameActivity) getActivity()).show(pic);
        } else {
            FragmentManager ft = getFragmentManager();
            FinishedGameFragment finishedGameFragment = new FinishedGameFragment();
            finishedGameFragment.show(ft, FinishedGameFragment.TAG);
        }
    }
}
