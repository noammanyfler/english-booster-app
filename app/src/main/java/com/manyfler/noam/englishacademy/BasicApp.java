package com.manyfler.noam.englishacademy;

import android.app.Application;
import android.content.Context;

import com.manyfler.noam.englishacademy.db.AppDatabase;
import com.manyfler.noam.englishacademy.db.FireDatabase;
import com.manyfler.noam.englishacademy.db.PicDataStorage;

/**
 * Created by noamm on 17-02-18.
 */

public class BasicApp extends Application {
    private static Context context;

    private AppExecutors mAppExecutors;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        mAppExecutors = new AppExecutors();
        BasicApp.getContext().getSharedPreferences("english_academy", Context.MODE_PRIVATE).edit().clear().apply();

    }

    public static Context getContext() {
        return context;
    }

    public AppDatabase getDatabase() {
        return AppDatabase.getInstance(this, mAppExecutors);
    }

    public FireDatabase getFireDatabase() {
        return FireDatabase.getInstance();
    }

    public PicDataStorage getPicDataStorage() {
        return PicDataStorage.getInstance();
    }

    public DataRepository getRepository() {
        return DataRepository.getInstance(getDatabase(), getFireDatabase(), getPicDataStorage(), mAppExecutors);
    }
}
