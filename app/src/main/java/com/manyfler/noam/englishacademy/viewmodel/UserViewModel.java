package com.manyfler.noam.englishacademy.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;

import com.manyfler.noam.englishacademy.BasicApp;
import com.manyfler.noam.englishacademy.DataRepository;
import com.manyfler.noam.englishacademy.db.entity.UserEntity;
import com.manyfler.noam.englishacademy.model.User;

/**
 * Created by noamm on 19-02-18.
 */

public class UserViewModel extends AndroidViewModel {
    private final LiveData<UserEntity> mObservableUser;

    public ObservableField<UserEntity> user = new ObservableField<>();

    private final String mUserUid;

    public UserViewModel(@NonNull Application application, DataRepository repository,
                            final String userUid) {
        super(application);
        mUserUid = userUid;

        mObservableUser = repository.getUser(mUserUid);
    }

    public LiveData<UserEntity> getObservableUser() {
        return mObservableUser;
    }

    public void setUser(UserEntity user) {
        this.user.set(user);
    }

    /**
     * A creator is used to inject the product ID into the ViewModel
     * <p>
     * This creator is to showcase how to inject dependencies into ViewModels. It's not
     * actually necessary in this case, as the product ID can be passed in a public method.
     */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        @NonNull
        private final Application mApplication;

        private final String mUserUid;

        private final DataRepository mRepository;

        /**
         * A creator is used to inject the product ID into the ViewModel
         * <p>
         * This creator is to showcase how to inject dependencies into ViewModels. It's not
         * actually necessary in this case, as the product ID can be passed in a public method.
         */
        public Factory(@NonNull Application application, String userUid) {
            mApplication = application;
            mUserUid = userUid;
            mRepository = ((BasicApp) application).getRepository();
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new UserViewModel(mApplication, mRepository, mUserUid);
        }
    }
}
