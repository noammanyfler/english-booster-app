package com.manyfler.noam.englishacademy.db;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

import com.manyfler.noam.englishacademy.AppExecutors;
import com.manyfler.noam.englishacademy.db.converter.DateConverter;
import com.manyfler.noam.englishacademy.db.dao.roomDao.PicDao;
import com.manyfler.noam.englishacademy.db.dao.roomDao.UserDao;
import com.manyfler.noam.englishacademy.db.entity.PicEntity;
import com.manyfler.noam.englishacademy.db.entity.UserEntity;

/**
 * Created by noamm on 16-02-18.
 */
@Database(entities = {UserEntity.class, PicEntity.class}, version = 6)
@TypeConverters(DateConverter.class)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase sInstance;
    private static final String DATABASE_NAME = "english-academy-db";

    public abstract UserDao userDao();

    public abstract PicDao picDao();

    private final MutableLiveData<Boolean> mIsDatabaseCreated = new MutableLiveData<>();

    public static AppDatabase getInstance(final Context context, final AppExecutors appExecutors) {
        if (sInstance == null) {
            synchronized (AppDatabase.class) {
                if (sInstance == null) {
                    sInstance = buildDatabase(context.getApplicationContext(), appExecutors);
                    sInstance.updateDatabaseCreated(context.getApplicationContext());
                }
            }
        }
        return sInstance;
    }

    /**
     * Build the database. {@link Builder#build()} only sets up the database configuration and
     * creates a new instance of the database.
     * The SQLite database is only created when it's accessed for the first time.
     */
    private static AppDatabase buildDatabase(final Context appContext,
                                             final AppExecutors executors) {
        return Room.databaseBuilder(appContext, AppDatabase.class, DATABASE_NAME)
                .addMigrations(new Migration(1, 2) {
                    @Override
                    public void migrate(@NonNull SupportSQLiteDatabase database) {
                        database.execSQL("CREATE TABLE pics(" +
                                "uid TEXT PRIMARY KEY NOT NULL," +
                                "pictureUrl TEXT," +
                                "hebrew TEXT," +
                                "english TEXT," +
                                "lastUpdated REAL NOT NULL" +
                                ")");
                    }
                }, new Migration(2, 3) {
                    @Override
                    public void migrate(@NonNull SupportSQLiteDatabase database) {
                        // DO nothing
                    }
                }, new Migration(3, 4) {
                    @Override
                    public void migrate(@NonNull SupportSQLiteDatabase database) {
                        database.execSQL("ALTER TABLE users ADD COLUMN isAdmin INTEGER");
                    }
                }, new Migration(4, 5) {
                    @Override
                    public void migrate(@NonNull SupportSQLiteDatabase database) {
                        database.execSQL("DROP TABLE users");
                        database.execSQL("CREATE TABLE users(" +
                                "uid TEXT PRIMARY KEY NOT NULL," +
                                "isAdmin INTEGER," +
                                "email TEXT," +
                                "score INTEGER," +
                                "lastUpdated REAL NOT NULL" +
                                ")");
                    }
                }, new Migration(5, 6) {
                   @Override
                   public void migrate(@NonNull SupportSQLiteDatabase database) {
                       database.execSQL("DROP TABLE users");
                       database.execSQL("CREATE TABLE users(" +
                               "uid TEXT PRIMARY KEY NOT NULL," +
                               "isAdmin INTEGER NOT NULL," +
                               "email TEXT," +
                               "score INTEGER NOT NULL," +
                               "lastUpdated REAL NOT NULL" +
                               ")");
                   }
                })
               .addCallback(new Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                    super.onCreate(db);
                    executors.diskIO().execute(() -> {
                        // Generate the data for pre-population
                        AppDatabase database = AppDatabase.getInstance(appContext, executors);
                        // notify that the database was created and it's ready to be used
                        database.setDatabaseCreated();
                        });
                    }
                }).build();
    }

    /**
     * Check whether the database already exists and expose it via {@link #getDatabaseCreated()}
     */
    private void updateDatabaseCreated(final Context context) {
        if (context.getDatabasePath(DATABASE_NAME).exists()) {
            setDatabaseCreated();
        }
    }


    public LiveData<Boolean> getDatabaseCreated() {
        return mIsDatabaseCreated;
    }

    private void setDatabaseCreated(){
        mIsDatabaseCreated.postValue(true);
    }

}
