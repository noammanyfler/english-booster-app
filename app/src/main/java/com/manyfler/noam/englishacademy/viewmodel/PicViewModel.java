package com.manyfler.noam.englishacademy.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.manyfler.noam.englishacademy.BasicApp;
import com.manyfler.noam.englishacademy.DataRepository;
import com.manyfler.noam.englishacademy.db.entity.PicEntity;

/**
 * Created by noamm on 28-02-18.
 */

public class PicViewModel extends AndroidViewModel {
    private final LiveData<PicEntity> mObservablePic;

    public ObservableField<PicEntity> pic = new ObservableField<>();

    private final String mPicUid;

    private DataRepository mRepository;

    public PicViewModel(@NonNull Application application, DataRepository repository, final String picUid) {
        super(application);
        mPicUid = picUid;
        mRepository = repository;
        mObservablePic = mRepository.getPic(mPicUid);
    }

    public LiveData<PicEntity> getObservablePic() {
        return mObservablePic;
    }

    public void setPic(PicEntity pic) {
        this.pic.set(pic);
    }

    public void addScoreToUser() {
        FirebaseUser fireUser = FirebaseAuth.getInstance().getCurrentUser();
        mRepository.addScore(fireUser);
    }


    /**
     * A creator is used to inject the product ID into the ViewModel
     * <p>
     * This creator is to showcase how to inject dependencies into ViewModels. It's not
     * actually necessary in this case, as the product ID can be passed in a public method.
     */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        @NonNull
        private final Application mApplication;

        private final String mPicUid;

        private final DataRepository mRepository;

        /**
         * A creator is used to inject the product ID into the ViewModel
         * <p>
         * This creator is to showcase how to inject dependencies into ViewModels. It's not
         * actually necessary in this case, as the product ID can be passed in a public method.
         */
        public Factory(@NonNull Application application, String picUid) {
            mApplication = application;
            mPicUid = picUid;
            mRepository = ((BasicApp) application).getRepository();
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new PicViewModel(mApplication, mRepository, mPicUid);
        }
    }
}
