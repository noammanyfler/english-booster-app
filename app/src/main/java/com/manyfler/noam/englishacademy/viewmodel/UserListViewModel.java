package com.manyfler.noam.englishacademy.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.content.Context;
import android.content.SharedPreferences;

import com.manyfler.noam.englishacademy.BasicApp;
import com.manyfler.noam.englishacademy.db.entity.UserEntity;

import java.util.List;

/**
 * Created by noamm on 17-02-18.
 */

public class UserListViewModel extends AndroidViewModel {
    private final MediatorLiveData<List<UserEntity>> mObservableUsers;

    public UserListViewModel(Application application) {
        super(application);

        mObservableUsers = new MediatorLiveData<>();
        mObservableUsers.setValue(null);

        LiveData<List<UserEntity>> users = ((BasicApp) application).getRepository().getAllUsers();

        mObservableUsers.addSource(users, mObservableUsers::setValue);
    }

    public LiveData<List<UserEntity>> getUsers() {
        return mObservableUsers;
    }
}
