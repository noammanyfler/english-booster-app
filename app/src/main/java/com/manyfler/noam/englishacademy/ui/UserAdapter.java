package com.manyfler.noam.englishacademy.ui;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.manyfler.noam.englishacademy.R;
import com.manyfler.noam.englishacademy.databinding.UserItemBinding;
import com.manyfler.noam.englishacademy.model.User;

import java.util.List;
import java.util.Objects;

/**
 * Created by noamm on 17-02-18.
 */

class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder>{

    List<? extends User> mUserList;

    @Nullable
    private final UserClickCallback mUserClickCallback;

    public UserAdapter(@Nullable UserClickCallback clickCallback) {
        mUserClickCallback = clickCallback;
    }

    public void setUserList(final List<? extends User> userList) {
        if (mUserList == null) {
            mUserList = userList;
            notifyItemRangeChanged(0, userList.size());
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return mUserList.size();
                }

                @Override
                public int getNewListSize() {
                    return userList.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return mUserList.get(oldItemPosition).getUid().equals(userList.get(newItemPosition).getUid());
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    User newUser = userList.get(newItemPosition);
                    User oldUser = mUserList.get(oldItemPosition);

                    return newUser.getUid().equals(oldUser.getUid())
                            && Objects.equals(newUser.getEmail(), oldUser.getEmail())
                            && Objects.equals(newUser.getLastUpdated(), oldUser.getLastUpdated())
                            && Objects.equals(newUser.getScore(), oldUser.getScore());
                }
            });
            mUserList = userList;
            result.dispatchUpdatesTo(this);
        }
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        UserItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.user_item,
                        parent, false);
        binding.setCallback(mUserClickCallback);
        return new UserViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        holder.binding.setUser(mUserList.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mUserList == null ? 0 : mUserList.size();
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {
        final UserItemBinding binding;

        public UserViewHolder(UserItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
