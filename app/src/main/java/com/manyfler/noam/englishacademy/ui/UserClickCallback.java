package com.manyfler.noam.englishacademy.ui;

import com.manyfler.noam.englishacademy.model.User;

/**
 * Created by noamm on 17-02-18.
 */

public interface UserClickCallback {
    void onClick(User product);
}
