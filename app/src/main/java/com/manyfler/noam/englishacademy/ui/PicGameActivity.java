package com.manyfler.noam.englishacademy.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;

import com.manyfler.noam.englishacademy.R;
import com.manyfler.noam.englishacademy.model.Pic;

/**
 * Created by noamm on 28-02-18.
 */

public class PicGameActivity extends AppCompatActivity implements AddScoreFragment.NoticeDialogListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pic_game);

        // Add product list fragment if this is first creation
        if (savedInstanceState == null) {
            PicGameFragment picGameFragment = new PicGameFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.pic_fragment_container, picGameFragment, picGameFragment.TAG).commit();
        }
    }

    @Override
    public void onDialogPositiveClick(android.support.v4.app.DialogFragment dialog) {
        switch (dialog.getTag()) {
            case AddScoreFragment.TAG:
                PicGameFragment picGameFragment = (PicGameFragment) getSupportFragmentManager().findFragmentByTag(PicGameFragment.TAG);
                picGameFragment.nextPic();
                break;
            case FinishedGameFragment.TAG:
                NavUtils.navigateUpFromSameTask(this);
                break;
        }
    }

    public void show(Pic pic) {
        PicFragment picFragment = PicFragment.forPic(pic.getUid());
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.child_fragment_container,
                        picFragment, null).commit();
    }
}
