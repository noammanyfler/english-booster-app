package com.manyfler.noam.englishacademy.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

/**
 * Created by noamm on 03-03-18.
 */

public class FinishedGameFragment extends android.support.v4.app.DialogFragment {
    public static final String TAG = "FinishedGameFragment";

    // Use this instance of the interface to deliver action events
    AddScoreFragment.NoticeDialogListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (AddScoreFragment.NoticeDialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("כל הכבוד! סיימת את כל התמונות להכתבה")
                    .setPositiveButton("חזור לתפריט הראשי", (dialog, which) -> {
                        mListener.onDialogPositiveClick(this);
                    });
        }

        // Create the AlertDialog object and return it
        return builder.create();
    }
}
