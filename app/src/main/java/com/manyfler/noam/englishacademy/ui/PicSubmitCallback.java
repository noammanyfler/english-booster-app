package com.manyfler.noam.englishacademy.ui;

import com.manyfler.noam.englishacademy.model.Pic;

/**
 * Created by noamm on 01-03-18.
 */

public interface PicSubmitCallback {
    void onClick(Pic pic);
}
